package ppss;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.categories.Category;
import ppss.Matricula;

/**
 *
 * @author mmc148
 */

@Category(CatSinParametros.class)
public class TestMatriculaSinParametros {


    public TestMatriculaSinParametros() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    
    @Test
    public void testC1() {
        Matricula mat = new Matricula();
      
       assertEquals(2000, mat.calculaTasaMatricula(20, false, true), 0.002f);
       
    }
    
     @Test
    public void testC2() {
        Matricula mat = new Matricula();
      
       assertEquals(250, mat.calculaTasaMatricula(70, false, true), 0.002f);
       
    }
    
     @Test
    public void testC3() {
        Matricula mat = new Matricula();
      
       assertEquals(250, mat.calculaTasaMatricula(20, true, true), 0.002f);
       
    }
    
     @Test
    public void testC4() {
        Matricula mat = new Matricula();
      
       assertEquals(500, mat.calculaTasaMatricula(20, false, false), 0.002f);
       
    }
    
     @Test
    public void testC5() {
        Matricula mat = new Matricula();
      
       assertEquals(400, mat.calculaTasaMatricula(60, false, true), 0.002f);
       
    }
}
