package ppss;


import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import ppss.Matricula;

/**
 *
 * @author mmc148
 */



@RunWith(Parameterized.class)
@Category(CatConParametros.class)
public class TestAlumnoConParametros {
    
    @Parameterized.Parameters(name = "Caso C{index}: validaNif({0})= {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            {"123", true},
            {"1234567AA", false},
            {"-12345678", false},
            {"00000000X", false},
            {"00000000T", true}
        });
    }
    private String dni;
    private boolean esperado;
   
    private Alumno alu = new Alumno();

    public TestAlumnoConParametros(String dni, boolean esperado) {
        this.dni = dni;
        this.esperado = esperado;
    }

    @Test
    public void testMatricula() {

        assertEquals(esperado, alu.validaNif(dni));

    }
}
