package ppss;


import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import ppss.Matricula;

/**
 *
 * @author mmc148
 */

@RunWith(Parameterized.class)
@Category(CatConParametros.class)
public class TestMatriculaConParametros {
    
    @Parameterized.Parameters(name = "Caso C{index}: calculaTasaMatricula({0},{1},{2})= {3}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            {20, false, true, 2000.00f},
            {70, false,true, 250.00f},
            {20, true, true, 250.00f},
            {20, false, false, 500.00f},
            {60, false, true, 400.00f}
        });
    }
    private int edad;
    private float esperado;
    private boolean repetidor, familia;
    private Matricula mat = new Matricula();

    public TestMatriculaConParametros(int edad, boolean familia, boolean repetidor, float esperado) {
        this.edad = edad;
        this.familia = familia;
        this.repetidor = repetidor;
        this.esperado = esperado;
    }

    @Test
    public void testMatricula() {

        assertEquals(esperado, mat.calculaTasaMatricula(edad, familia, repetidor), 002f);

    }
}
