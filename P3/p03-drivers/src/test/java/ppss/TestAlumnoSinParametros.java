/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.categories.Category;

/**
 *
 * @author ppss
 */
@Category(CatSinParametros.class)
public class TestAlumnoSinParametros {

    public TestAlumnoSinParametros() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
       
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testC1() {
        Alumno alu = new Alumno();
        assertEquals(false, alu.validaNif("123"));

    }

    @Test
    public void testC2() {
        Alumno alu = new Alumno();
        assertEquals(false, alu.validaNif("1234567AA"));

    }

    @Test
    public void testC3() {
        Alumno alu = new Alumno();
        assertEquals(false, alu.validaNif("-12345678"));

    }

    @Test
    public void testC4() {
        Alumno alu = new Alumno();
        assertEquals(false, alu.validaNif("00000000X"));

    }

    @Test
    public void testC5() {
        Alumno alu = new Alumno();
        assertEquals(true, alu.validaNif("00000000T"));

    }

}
