/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.easymock.EasyMock;
import org.junit.Test;
import static org.junit.Assert.*;
import ppss.Calendario;
import ppss.GestorLlamadas;

/**
 *
 * @author mmc148
 */
public class GestorLlamadasMockTest {
    
    public GestorLlamadasMockTest() {
    }

    @Test
    public void testCalculaConsumoC1() {
        double esperado = 208;
        
        Calendario cal = EasyMock.createMock(Calendario.class);
        EasyMock.expect(cal.getHoraActual()).andReturn(15);

        GestorLlamadas ges = EasyMock.partialMockBuilder(GestorLlamadas.class).addMockedMethod("getCalendario").createMock();
        EasyMock.expect(ges.getCalendario()).andReturn(cal);
        EasyMock.replay(cal);
        EasyMock.replay(ges);
        
        
        double resultado = ges.calculaConsumo(10);
        
        assertEquals(esperado,resultado, 0.002);
        
        EasyMock.verify(cal);
        EasyMock.verify(ges);
    }
    
    @Test
    public void testCalculaConsumoC2() {
        double esperado = 105;
        
        Calendario cal = EasyMock.createMock(Calendario.class);
        EasyMock.expect(cal.getHoraActual()).andReturn(22);

        GestorLlamadas ges = EasyMock.partialMockBuilder(GestorLlamadas.class).addMockedMethod("getCalendario").createMock();
        EasyMock.expect(ges.getCalendario()).andReturn(cal);
        EasyMock.replay(cal);
        EasyMock.replay(ges);
        
        
        double resultado = ges.calculaConsumo(10);
        
        assertEquals(esperado,resultado, 0.002);
        
        EasyMock.verify(cal);
        EasyMock.verify(ges);
    }

}
