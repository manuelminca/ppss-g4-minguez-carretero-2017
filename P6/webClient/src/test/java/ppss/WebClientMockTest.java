package ppss;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.sun.net.ssl.HttpsURLConnection;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.easymock.EasyMock;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import org.junit.Test;
import static org.junit.Assert.*;
import ppss.WebClient;
import ppss.WebClient;

/**
 *
 * @author Minguez
 */
public class WebClientMockTest {
    
    public WebClientMockTest() {
    }
    
    
    @Test
    public void testGetContentC1() throws IOException{
        
        WebClient cli = EasyMock.partialMockBuilder(WebClient.class).addMockedMethod("createHttpURLConnection").createMock();
        HttpURLConnection con = EasyMock.createMock(HttpURLConnection.class);
        
        URL dir = new URL ("http://www.ua.es");
        InputStream input = new ByteArrayInputStream("Funciona".getBytes());
        EasyMock.expect(con.getInputStream()).andReturn(input);
       
        EasyMock.expect(cli.createHttpURLConnection(dir)).andReturn(con);
        
        replay(cli);
        replay(con);
        
        String esperado = "Funciona";
        
        assertEquals(esperado, cli.getContent(dir));
        
        verify(cli);
        verify(con);
        
    }
    
    @Test
    public void testGetContentC2() throws IOException{
        
        WebClient cli = EasyMock.partialMockBuilder(WebClient.class).addMockedMethod("createHttpURLConnection").createMock();
        HttpURLConnection con = EasyMock.createMock(HttpURLConnection.class);
        
        URL dir = new URL ("http://www.ua.es");
        EasyMock.expect(con.getInputStream()).andThrow(new IOException());
       
        EasyMock.expect(cli.createHttpURLConnection(dir)).andReturn(con);
        
        replay(cli);
        replay(con);
        
        String esperado = null;
        
        assertEquals(esperado, cli.getContent(dir));
        
        verify(cli);
        verify(con);
        
    }
    
    
}
