/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio4;

import org.easymock.EasyMock;
import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.anyString;
import org.junit.Test;
import static org.junit.Assert.*;
import ppss.ejercicio4.excepciones.IsbnInvalidoException;
import ppss.ejercicio4.excepciones.JDBCException;
import ppss.ejercicio4.excepciones.ReservaException;
import ppss.ejercicio4.excepciones.SocioInvalidoException;

/**
 *
 * @author Minguez
 */
public class ReservaStubTest {

    public ReservaStubTest() {
    }

    @Test
    public void testRealizaReservaC1() throws ReservaException, IsbnInvalidoException, SocioInvalidoException, JDBCException {
        Reserva res = EasyMock.partialMockBuilder(Reserva.class).addMockedMethods("compruebaPermisos", "getFactoriaBOs").createNiceMock();
        EasyMock.expect(res.compruebaPermisos(anyString(), anyString(), anyObject())).andStubReturn(false);
        EasyMock.replay(res);
        String[] isbns = {"11111"};
        try {
            res.realizaReserva("xxxx", "xxxx", "Luis", isbns);
        } catch (ReservaException e) {
            String esperado = "ERROR de permisos; ";
            String real = e.getMessage();
            assertEquals(esperado, real);
        }

    }

    @Test
    public void testRealizaReservaC2() throws ReservaException, IsbnInvalidoException, SocioInvalidoException, JDBCException {
        Reserva res = EasyMock.partialMockBuilder(Reserva.class).addMockedMethods("compruebaPermisos", "getFactoriaBOs").createNiceMock();
        EasyMock.expect(res.compruebaPermisos(anyString(), anyString(), anyObject())).andStubReturn(true);

        IOperacionBO io = EasyMock.createNiceMock(IOperacionBO.class);

        FactoriaBOs fac = EasyMock.createNiceMock(FactoriaBOs.class);
        EasyMock.expect(fac.getOperacionBO()).andStubReturn(io);

        EasyMock.expect(res.getFactoriaBOs()).andStubReturn(fac);

        EasyMock.replay(io);
        EasyMock.replay(fac);
        EasyMock.replay(res);

        String[] isbns = {"11111", "22222"};
        
        try{
            res.realizaReserva("ppss", "ppss", "Luis", isbns);
        }
        catch(Exception e){
            fail("no deberia fallar");
        }

  
    }

    @Test
    public void testRealizaReservaC3() throws ReservaException, IsbnInvalidoException, SocioInvalidoException, JDBCException {

        Reserva res = EasyMock.partialMockBuilder(Reserva.class).addMockedMethods("compruebaPermisos", "getFactoriaBOs").createNiceMock();
        EasyMock.expect(res.compruebaPermisos(anyString(), anyString(), anyObject())).andStubReturn(true);

        IOperacionBO io = EasyMock.createNiceMock(IOperacionBO.class);

        io.operacionReserva("Luis", "33333");
        EasyMock.expectLastCall().andThrow(new IsbnInvalidoException());
 

        FactoriaBOs fac = EasyMock.createNiceMock(FactoriaBOs.class);
        EasyMock.expect(fac.getOperacionBO()).andStubReturn(io);

        EasyMock.expect(res.getFactoriaBOs()).andStubReturn(fac);

        EasyMock.replay(io);
        EasyMock.replay(fac);
        EasyMock.replay(res);

        String[] isbns = {"33333"};
        try {
            res.realizaReserva("ppss", "ppss", "Luis", isbns);
        } catch (ReservaException e) {
            String esperado = "ISBN invalido:33333; ";
            String real = e.getMessage();
            assertEquals(esperado, real);
        }
       
    }
    


    @Test
    public void testRealizaReservaC4() throws ReservaException, IsbnInvalidoException, SocioInvalidoException, JDBCException {
        Reserva res = EasyMock.partialMockBuilder(Reserva.class).addMockedMethods("compruebaPermisos", "getFactoriaBOs").createNiceMock();
        EasyMock.expect(res.compruebaPermisos(anyString(), anyString(), anyObject())).andStubReturn(true);

        IOperacionBO io = EasyMock.createNiceMock(IOperacionBO.class);

        io.operacionReserva("Luis", "11111");
        EasyMock.expectLastCall().andThrow(new SocioInvalidoException());

        FactoriaBOs fac = EasyMock.createNiceMock(FactoriaBOs.class);
        EasyMock.expect(fac.getOperacionBO()).andStubReturn(io);

        EasyMock.expect(res.getFactoriaBOs()).andStubReturn(fac);

        EasyMock.replay(io);
        EasyMock.replay(fac);
        EasyMock.replay(res);

        String[] isbns = {"11111"};
        try {
            res.realizaReserva("ppss", "ppss", "Luis", isbns);
        } catch (ReservaException e) {
            String esperado = "SOCIO invalido; ";
            String real = e.getMessage();
            assertEquals(esperado, real);
        }
       
    }

        @Test
    public void testRealizaReservaC5() throws ReservaException, IsbnInvalidoException, SocioInvalidoException, JDBCException {
        Reserva res = EasyMock.partialMockBuilder(Reserva.class).addMockedMethods("compruebaPermisos", "getFactoriaBOs").createNiceMock();
        EasyMock.expect(res.compruebaPermisos(anyString(), anyString(), anyObject())).andStubReturn(true);

        IOperacionBO io = EasyMock.createNiceMock(IOperacionBO.class);

        io.operacionReserva("Luis", "11111");
        EasyMock.expectLastCall().andThrow(new JDBCException());

        FactoriaBOs fac = EasyMock.createNiceMock(FactoriaBOs.class);
        EasyMock.expect(fac.getOperacionBO()).andStubReturn(io);

        EasyMock.expect(res.getFactoriaBOs()).andStubReturn(fac);

        EasyMock.replay(io);
        EasyMock.replay(fac);
        EasyMock.replay(res);

        String[] isbns = {"11111"};
        try {
            res.realizaReserva("ppss", "ppss", "Luis", isbns);
        } catch (ReservaException e) {
            String esperado = "CONEXION invalida; ";
            String real = e.getMessage();
            assertEquals(esperado, real);
        }
       
    }
}
