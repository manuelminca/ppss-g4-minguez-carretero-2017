/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio4;

import java.util.Iterator;
import org.easymock.EasyMock;

import org.junit.Test;
import static org.junit.Assert.*;
import ppss.ejercicio4.excepciones.IsbnInvalidoException;
import ppss.ejercicio4.excepciones.JDBCException;
import ppss.ejercicio4.excepciones.ReservaException;
import ppss.ejercicio4.excepciones.SocioInvalidoException;

/**
 *
 * @author Minguez
 */
public class ReservaMockTest {

    public ReservaMockTest() {
    }

    @Test
    public void testRealizaReservaC1() {
        Reserva res = EasyMock.partialMockBuilder(Reserva.class).addMockedMethods("compruebaPermisos").createMock();
        try {

            EasyMock.expect(res.compruebaPermisos("xxxx", "xxxx", Reserva.Usuario.BIBLIOTECARIO)).andReturn(Boolean.FALSE);

            String[] isbns = {"11111"};
            EasyMock.replay(res);

            res.realizaReserva("xxxx", "xxxx", "Luis", isbns);
        } catch (ReservaException exc) {
            String esperado = "ERROR de permisos; ";
            String real = exc.getMessage();
            assertEquals(esperado, real);
            EasyMock.verify(res);
        } catch (IsbnInvalidoException e) {
            fail("no deberia fallar");
        } catch (JDBCException e) {
            fail("no deberia fallar");
        } catch (SocioInvalidoException e) {
            fail("no deberia fallar");
        }

    }

    @Test
    public void testRealizaReservaC2() {
        Reserva res = EasyMock.partialMockBuilder(Reserva.class).addMockedMethods("compruebaPermisos", "getFactoriaBOs").createMock();
        IOperacionBO io = EasyMock.createStrictMock(IOperacionBO.class);
        FactoriaBOs fac = EasyMock.createMock(FactoriaBOs.class);
        try {

            EasyMock.expect(res.compruebaPermisos("ppss", "ppss", Reserva.Usuario.BIBLIOTECARIO)).andReturn(Boolean.TRUE);

            EasyMock.expect(fac.getOperacionBO()).andReturn(io);
            EasyMock.expect(res.getFactoriaBOs()).andReturn(fac);

            String[] isbns = {"11111", "22222"};

            for (int i = 0; i < isbns.length; i++) {
                io.operacionReserva("Luis", isbns[i]);
            }

            EasyMock.replay(io);
            EasyMock.replay(fac);
            EasyMock.replay(res);

            res.realizaReserva("ppss", "ppss", "Luis", isbns);
            assertTrue(true);

        } catch (ReservaException e) {
            fail("no deberia fallar");
        } catch (IsbnInvalidoException e) {
            fail("no deberia fallar");
        } catch (JDBCException e) {
            fail("no deberia fallar");
        } catch (SocioInvalidoException e) {
            fail("no deberia fallar");
        }

        EasyMock.verify(res);
        EasyMock.verify(fac);
        EasyMock.verify(io);

    }

    @Test
    public void testRealizaReservaC3(){
        String[] isbns = {"33333"};
        Reserva res = EasyMock.partialMockBuilder(Reserva.class).addMockedMethods("compruebaPermisos", "getFactoriaBOs").createMock();
        IOperacionBO io = EasyMock.createMock(IOperacionBO.class);
        FactoriaBOs fac = EasyMock.createMock(FactoriaBOs.class);

        try {

            EasyMock.expect(res.compruebaPermisos("ppss", "ppss", Reserva.Usuario.BIBLIOTECARIO)).andReturn(Boolean.TRUE);

            io.operacionReserva("Luis", "33333");
            EasyMock.expectLastCall().andThrow(new IsbnInvalidoException());
            EasyMock.expect(fac.getOperacionBO()).andReturn(io);
            EasyMock.expect(res.getFactoriaBOs()).andReturn(fac);

            EasyMock.replay(io);
            EasyMock.replay(fac);
            EasyMock.replay(res);

            res.realizaReserva("ppss", "ppss", "Luis", isbns);
            fail("deberia fallar");
        } catch (ReservaException e) {
            String esperado = "ISBN invalido:33333; ";
            String real = e.getMessage();

            assertEquals(esperado, real);

            EasyMock.verify(res);
            EasyMock.verify(fac);
            EasyMock.verify(io);
        } catch (IsbnInvalidoException e) {
            fail("no deberia fallar");
        } catch (JDBCException e) {
            fail("no deberia fallar");
        } catch (SocioInvalidoException e) {
            fail("no deberia fallar");
        }

    }

    @Test
    public void testRealizaReservaC4() {
        IOperacionBO io = EasyMock.createMock(IOperacionBO.class);
        FactoriaBOs fac = EasyMock.createMock(FactoriaBOs.class);
        String[] isbns = {"11111"};
        Reserva res = EasyMock.partialMockBuilder(Reserva.class).addMockedMethods("compruebaPermisos", "getFactoriaBOs").createMock();

        try {
            EasyMock.expect(res.compruebaPermisos("ppss", "ppss", Reserva.Usuario.BIBLIOTECARIO)).andReturn(Boolean.TRUE);

            io.operacionReserva("Pepe", "11111");
            EasyMock.expectLastCall().andThrow(new SocioInvalidoException());
            EasyMock.expect(fac.getOperacionBO()).andReturn(io);
            EasyMock.expect(res.getFactoriaBOs()).andReturn(fac);

            EasyMock.replay(io);
            EasyMock.replay(fac);
            EasyMock.replay(res);

            res.realizaReserva("ppss", "ppss", "Pepe", isbns);
            fail("deberia fallar");
        } catch (ReservaException e) {
            String esperado = "SOCIO invalido; ";
            String real = e.getMessage();

            assertEquals(esperado, real);

            EasyMock.verify(res);
            EasyMock.verify(fac);
            EasyMock.verify(io);
        } catch (IsbnInvalidoException e) {
            fail("no deberia fallar");
        } catch (JDBCException e) {
            fail("no deberia fallar");
        } catch (SocioInvalidoException e) {
            fail("no deberia fallar");
        }

    }

    @Test
    public void testRealizaReservaC5() {
        String[] isbns = {"11111"};
        Reserva res = EasyMock.partialMockBuilder(Reserva.class).addMockedMethods("compruebaPermisos", "getFactoriaBOs").createMock();
        IOperacionBO io = EasyMock.createMock(IOperacionBO.class);
        FactoriaBOs fac = EasyMock.createMock(FactoriaBOs.class);

        try {

            EasyMock.expect(res.compruebaPermisos("ppss", "ppss", Reserva.Usuario.BIBLIOTECARIO)).andReturn(Boolean.TRUE);

            io.operacionReserva("Luis", "11111");
            EasyMock.expectLastCall().andThrow(new JDBCException());
            EasyMock.expect(fac.getOperacionBO()).andReturn(io);
            EasyMock.expect(res.getFactoriaBOs()).andReturn(fac);

            EasyMock.replay(io);
            EasyMock.replay(fac);
            EasyMock.replay(res);

            res.realizaReserva("ppss", "ppss", "Luis", isbns);
            fail("deberia fallar");
        } catch (ReservaException e) {
            String esperado = "CONEXION invalida; ";
            String real = e.getMessage();

            assertEquals(esperado, real);

            EasyMock.verify(res);
            EasyMock.verify(fac);
            EasyMock.verify(io);
        } catch (IsbnInvalidoException e) {
            fail("no deberia fallar");
        } catch (JDBCException e) {
            fail("no deberia fallar");
        } catch (SocioInvalidoException e) {
            fail("no deberia fallar");
        }

    }

}
