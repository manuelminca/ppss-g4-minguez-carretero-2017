/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.easymock.EasyMock;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.verify;
import org.junit.Test;
import static org.junit.Assert.*;
import ppss.Listados;

/**
 *
 * @author Minguez
 */
public class ListadosTest {
    String apellido1, apellido2, nombre, tabla,esperado, real;
    
    public ListadosTest() {
    }
    
    @Test
    public void testListadosC1() throws SQLException{
        
        apellido1 = "Garcia";
        apellido2 = "Molina";
        nombre = "Ana";
        tabla = "tabla";
        esperado = "Garcia, Molina, Ana\nLacalle, Verna, Jose Luis\n";
        ResultSet rs = EasyMock.createStrictMock(ResultSet.class);
        
        expect(rs.next()).andReturn(Boolean.TRUE);

        expect(rs.getString("apellido1")).andReturn("Garcia");
        expect(rs.getString("apellido2")).andReturn("Molina");
        expect(rs.getString("nombre")).andReturn("Ana");
        
        expect(rs.next()).andReturn(Boolean.TRUE);
        expect(rs.getString("apellido1")).andReturn("Lacalle");
        expect(rs.getString("apellido2")).andReturn("Verna");
        expect(rs.getString("nombre")).andReturn("Jose Luis");
        expect(rs.next()).andReturn(Boolean.FALSE);
        

        Statement stm = EasyMock.createMock(Statement.class);
        
        expect(stm.executeQuery("SELECT apellido1, apellido2, nombre FROM tabla")).andReturn(rs);
        
        
        Connection con = EasyMock.createMock(Connection.class);
        expect(con.createStatement()).andReturn(stm);
        
        EasyMock.replay(rs);
        EasyMock.replay(stm);
        EasyMock.replay(con);
        
        Listados list = new Listados();
        real = list.porApellidos(con, tabla);
        assertEquals(esperado, real);
        
        verify(rs);
        verify(stm);
        verify(con);
        
    }
    
    @Test
    public void testListadosC2() throws SQLException{
        
        tabla = "tabla";
        Statement stm = createMock(Statement.class);
        Connection con = createMock(Connection.class);
        ResultSet res = createMock(ResultSet.class);
        
        expect(res.next()).andThrow(new SQLException());
        
        expect(stm.executeQuery("SELECT apellido1, apellido2, nombre FROM tabla")).andReturn(res);
        expect(con.createStatement()).andReturn(stm);
        
        EasyMock.replay(res);
        EasyMock.replay(stm);
        EasyMock.replay(con);
        
        Listados lista = new Listados();
        
        
        
        try {
            lista.porApellidos(con, tabla);
            fail("deberia fallar");
        } catch (SQLException e) {
            assertTrue(true);
        }     
    }
}
