/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 
package tests;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.easymock.EasyMock;
import static org.easymock.EasyMock.anyString;
import static org.easymock.EasyMock.expect;
import org.junit.Test;
import static org.junit.Assert.*;
import ppss.Listados;


public class ListadosStubTest {
    
    public ListadosStubTest() {
    }
    
    @Test
    public void testListadosC1(){
        
        Connection con = EasyMock.createNiceMock(Connection.class);
        Statement stm = EasyMock.createNiceMock(Statement.class);
        ResultSet res = EasyMock.createNiceMock(ResultSet.class);
        
        try{

            
            expect(res.next()).andStubReturn(Boolean.TRUE);
            expect(res.getString(anyString())).andStubReturn("Garcia");
            expect(res.getString(anyString())).andStubReturn("Molina");
            expect(res.getString(anyString())).andStubReturn("Ana");
            
            expect(res.next()).andStubReturn(Boolean.TRUE);
            expect(res.getString(anyString())).andStubReturn("Lacalle");
            expect(res.getString(anyString())).andStubReturn("Verna");
            expect(res.getString(anyString())).andStubReturn("Jose Luis");

            expect(res.next()).andStubReturn(Boolean.FALSE);            
            
            expect(stm.executeQuery(anyString())).andStubReturn(res);
            expect(con.createStatement()).andStubReturn(stm);            
            
            
            EasyMock.replay(res);
            EasyMock.replay(stm);
            EasyMock.replay(con);
        
            
            Listados list = new Listados();
            String real = list.porApellidos(con, "tabla");
            String esperado = "Garcia, Molina, Ana\nLacalle, Verna, Jose Luis\n";
            assertEquals(esperado, real);
        }catch (SQLException e){
            fail("No deberia fallar");
        }
          
    }
    
    
}
*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.easymock.EasyMock;
import static org.easymock.EasyMock.anyString;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.verify;
import org.junit.Test;
import static org.junit.Assert.*;
import ppss.Listados;

/**
 *
 * @author Minguez
 */
public class ListadosStubTest {
    String esperado, real;
    
    public ListadosStubTest() {
    }
    
    @Test
    public void testListadosC1() throws SQLException{
        
       
        esperado = "Garcia, Molina, Ana\nLacalle, Verna, Jose Luis\n";
        ResultSet rs = EasyMock.createNiceMock(ResultSet.class);
        
        expect(rs.next()).andReturn(Boolean.TRUE);
        expect(rs.getString(anyString())).andReturn("Garcia");
        expect(rs.getString(anyString())).andReturn("Molina");
        expect(rs.getString(anyString())).andReturn("Ana");
        
        expect(rs.next()).andReturn(Boolean.TRUE);
        expect(rs.getString(anyString())).andReturn("Lacalle");
        expect(rs.getString(anyString())).andReturn("Verna");
        expect(rs.getString(anyString())).andReturn("Jose Luis");
        expect(rs.next()).andReturn(Boolean.FALSE);
        

        Statement stm = EasyMock.createNiceMock(Statement.class);
        
        expect(stm.executeQuery(anyString())).andStubReturn(rs);
        
        
        Connection con = EasyMock.createNiceMock(Connection.class);
        expect(con.createStatement()).andStubReturn(stm);
        
        EasyMock.replay(rs);
        EasyMock.replay(stm);
        EasyMock.replay(con);
        
        Listados list = new Listados();
        String real = list.porApellidos(con, "tabla");
        assertEquals(esperado, real);
        
        
    }
    
}

