/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.matriculacion.dao;

import java.util.Calendar;
import org.junit.Test;
import static org.junit.Assert.*;
import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.util.fileloader.DataFileLoader;
import org.dbunit.util.fileloader.FlatXmlDataFileLoader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.apache.log4j.BasicConfigurator;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import static org.junit.Assert.assertEquals;
import ppss.matriculacion.to.AlumnoTO;

/**
 *
 * @author ppss
 */
public class AlumnoDAOIT {

    IDataSet dataSet;
    private IDatabaseTester databaseTester;

    @BeforeClass
    public static void only_once() {
//    Para evitar el mensaje   
//    Running ppss.dbunitexample.TestDBUnit
//    log4j:WARN No appenders could be found for logger (org.dbunit.assertion.SimpleAssert).
//    log4j:WARN Please initialize the log4j system properly.  
        BasicConfigurator.configure();

    }

    @Before
    public void setUp() throws Exception {
        databaseTester = new JdbcDatabaseTester("com.mysql.jdbc.Driver",
                "jdbc:mysql://localhost:3306/matriculacion", "root", "ppss");
        DataFileLoader loader = new FlatXmlDataFileLoader();
        dataSet = loader.load("/customer-init.xml");
        databaseTester.setDataSet(dataSet);
        databaseTester.onSetup();

    }

    @Test
    public void testA1() throws Exception {

        FactoriaDAO factoriaDAO = new FactoriaDAO();
        IAlumnoDAO alumnoDAO = factoriaDAO.getAlumnoDAO();

        AlumnoTO alum = new AlumnoTO();
        alum.setNif("33333333C");
        alum.setNombre("Elena Aguirre Juarez");

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, 1985);
        cal.set(Calendar.MONTH, 01); // Nota: en la clase Calendar, el primer mes es 0
        cal.set(Calendar.DATE, 22);
        alum.setFechaNacimiento(cal.getTime());

        try {
            alumnoDAO.addAlumno(alum);
        } catch (Exception e) {
            fail("No deberia fallar");
        }

        IDatabaseConnection connection = databaseTester.getConnection();

        // configuramos la conexión como de tipo mysql
        // para evitar el mensaje: 
        // WARN org.dbunit.dataset.AbstractTableMetaData - Potential problem found: 
        //      The configured data type factory 'class org.dbunit.dataset.datatype.DefaultDataTypeFactory' 
        //      might cause problems with the current database 'MySQL' (e.g. some datatypes may not be supported properly). 
        //      ...
        DatabaseConfig dbconfig = connection.getConfig();
        dbconfig.setProperty("http://www.dbunit.org/properties/datatypeFactory", new MySqlDataTypeFactory());

        IDataSet databaseDataSet = connection.createDataSet();
        ITable actualTable = databaseDataSet.getTable("alumnos");

        DataFileLoader loader2 = new FlatXmlDataFileLoader();
        IDataSet expectedDataSet = loader2.load("/tabla3.xml");

        ITable expectedTable = expectedDataSet.getTable("alumnos");

        Assertion.assertEquals(expectedTable, actualTable);

    }

    @Test
    public void testA2() {
        FactoriaDAO factoriaDAO = new FactoriaDAO();
        IAlumnoDAO alumnoDAO = factoriaDAO.getAlumnoDAO();

        AlumnoTO alum = new AlumnoTO();
        alum.setNif("11111111A");
        alum.setNombre("Alfonso Ramirez Ruiz");

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, 1982);
        cal.set(Calendar.MONTH, 01); // Nota: en la clase Calendar, el primer mes es 0
        cal.set(Calendar.DATE, 22);
        alum.setFechaNacimiento(cal.getTime());

        try {
            alumnoDAO.addAlumno(alum);
            fail("No deberia fallar");
        } catch (DAOException e) {
            //deberia entrar aqui
        }
    }

    @Test
    public void testA3() {
        FactoriaDAO factoriaDAO = new FactoriaDAO();
        IAlumnoDAO alumnoDAO = factoriaDAO.getAlumnoDAO();

        AlumnoTO alum = new AlumnoTO();
        alum.setNif("44444444D");
        alum.setNombre(null);

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, 1982);
        cal.set(Calendar.MONTH, 01); // Nota: en la clase Calendar, el primer mes es 0
        cal.set(Calendar.DATE, 22);
        alum.setFechaNacimiento(cal.getTime());

        try {
            alumnoDAO.addAlumno(alum);
            fail("No deberia fallar");
        } catch (DAOException e) {
            //deberia entrar aqui
        }
    }

    @Test
    public void testA4() {
        FactoriaDAO factoriaDAO = new FactoriaDAO();
        IAlumnoDAO alumnoDAO = factoriaDAO.getAlumnoDAO();

        AlumnoTO alum = null;

        try {
            alumnoDAO.addAlumno(alum);
            fail("No deberia fallar");
        } catch (DAOException e) {
            //deberia entrar aqui
        }
    }

    @Test
    public void testA5() {
        FactoriaDAO factoriaDAO = new FactoriaDAO();
        IAlumnoDAO alumnoDAO = factoriaDAO.getAlumnoDAO();

        AlumnoTO alum = new AlumnoTO();
        alum.setNif(null);
        alum.setNombre("Pedro Garcia Lopez");

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, 1982);
        cal.set(Calendar.MONTH, 01); // Nota: en la clase Calendar, el primer mes es 0
        cal.set(Calendar.DATE, 22);
        alum.setFechaNacimiento(cal.getTime());

        try {
            alumnoDAO.addAlumno(alum);
            fail("No deberia fallar");
        } catch (DAOException e) {
            //deberia entrar aqui
        }
    }

    @Test
    public void testB1() throws Exception {
        FactoriaDAO factoriaDAO = new FactoriaDAO();
        IAlumnoDAO alum = factoriaDAO.getAlumnoDAO();

        try {
            alum.delAlumno("11111111A");
            
            IDatabaseConnection connection = databaseTester.getConnection();
            IDataSet dataSetActual = connection.createDataSet();

            DataFileLoader loader = new FlatXmlDataFileLoader();
            IDataSet expectedDataSet = loader.load("/tabla4.xml");
            
            ITable tablaEsperada = expectedDataSet.getTable("alumnos");
            ITable tablaReal = dataSetActual.getTable("alumnos");
            Assertion.assertEquals(tablaEsperada,tablaReal);
        } catch (DAOException e) {
            fail("no deberia fallar");
        }

    }
    
    @Test
    public void testB2() throws Exception {
        FactoriaDAO factoriaDAO = new FactoriaDAO();
        IAlumnoDAO alum = factoriaDAO.getAlumnoDAO();

        try {
            alum.delAlumno("33333333C");
            fail("no deberia fallar");
        } catch (DAOException e) {
            
        }

    }

}
