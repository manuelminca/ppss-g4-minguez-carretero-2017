/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mmc148
 */
public class DataArrayTest {

    DataArray dat = new DataArray();
    int[] resultadoEsperado = new int[10];
    int[] resultadoReal;

    @Test
    public void testDataArrayC1() {

        for (int i = 0; i < resultadoEsperado.length; i++) {
            resultadoEsperado[i] = 0;
        }

        resultadoEsperado[0] = 1;
        resultadoEsperado[1] = 5;
        resultadoEsperado[2] = 7;
        resultadoEsperado[3] = 8;
        

        dat.add(1);
        dat.add(5);
        dat.add(7);
        dat.add(8);

        resultadoReal = dat.delete(9);
        Assert.assertArrayEquals(resultadoEsperado, resultadoReal);
    }
    
    @Test
    public void testDataArrayC2() {

        for (int i = 0; i < resultadoEsperado.length; i++) {
            resultadoEsperado[i] = 0;
        }

        resultadoEsperado[0] = 1;
        resultadoEsperado[1] = 7;
        resultadoEsperado[2] = 8;
        

        dat.add(1);
        dat.add(5);
        dat.add(7);
        dat.add(8);

        resultadoReal = dat.delete(5);
        Assert.assertArrayEquals(resultadoEsperado, resultadoReal);
    }
    
    @Test
    public void testDataArrayC3() {

        for (int i = 0; i < resultadoEsperado.length; i++) {
            resultadoEsperado[i] = 0;
        }

        resultadoEsperado[0] = 1;
        resultadoEsperado[1] = 7;
        
        dat.add(1);
        dat.add(7);
        dat.add(8);

        resultadoReal = dat.delete(8);
        Assert.assertArrayEquals(resultadoEsperado, resultadoReal);
    }

        @Test
    public void testDataArrayC4() {

        for (int i = 0; i < resultadoEsperado.length; i++) {
            resultadoEsperado[i] = 0;
        }

        resultadoEsperado[0] = 1;
        resultadoEsperado[1] = 2;
        resultadoEsperado[2] = 3;
        resultadoEsperado[3] = 4;
        resultadoEsperado[4] = 5;
        resultadoEsperado[5] = 6;
        resultadoEsperado[6] = 7;
        resultadoEsperado[7] = 8;
        resultadoEsperado[8] = 9;
        
        dat.add(1);
        dat.add(2);
        dat.add(3);
        dat.add(4);
        dat.add(5);
        dat.add(6);
        dat.add(7);
        dat.add(8);
        dat.add(9);
        dat.add(10);
             

        resultadoReal = dat.delete(10);
        Assert.assertArrayEquals(resultadoEsperado, resultadoReal);
    }
 


    public DataArrayTest() {

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of delete method, of class DataArray.
     */
}
