package ppss;


public class DataArray {
    private int[] coleccion;
    private int numElem;
    
    //Constructor
    public DataArray() {
        coleccion = new int[10];
        numElem=0;        
    }
    
    public int size() {
        return numElem;
    }
    
    //método para añadir un entero a la colección
    public void add(int elem) {
    if (numElem < (coleccion.length)) {
            coleccion[numElem]= elem;
            numElem++;
            System.out.println("added "+elem +" ahora hay "+numElem+ " elementos");
        } else {
            System.out.println(elem +" ya no cabe. Ya has añadido "+numElem+" elementos");
        } 
    }
    

    //método para borrar un entero a la colección
    public int[] delete(int elem) { 
        
       for(int i=0; i< coleccion.length; i++){
           if(coleccion[i] == elem){
               for(int j=i; j< coleccion.length -1 ;j++){
                   coleccion[j]=coleccion[j+1];
               }
               if(numElem==10){
                   coleccion[numElem-1]=0;
               }
           }
       }
        
        return coleccion;
    }
    
}
