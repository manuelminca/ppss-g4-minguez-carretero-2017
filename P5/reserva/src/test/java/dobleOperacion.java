
import ppss.ejercicio3.IOperacionBO;
import ppss.ejercicio3.excepciones.IsbnInvalidoException;
import ppss.ejercicio3.excepciones.JDBCException;
import ppss.ejercicio3.excepciones.SocioInvalidoException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Minguez
 */
public class dobleOperacion implements IOperacionBO {
    private int num;

    public void setNum (int e){
        num = e;
    }
    @Override
    public void operacionReserva(String socio, String isbn) throws IsbnInvalidoException, JDBCException, SocioInvalidoException {
       switch (num){
           case 2:
               throw new IsbnInvalidoException();
           case 3:
               throw new SocioInvalidoException();
           case 4:
               throw new JDBCException();

           default:
               break;
       }
    }
    
}
