
import ppss.ejercicio3.IOperacionBO;
import ppss.ejercicio3.Operacion;
import ppss.ejercicio3.Reserva;
import ppss.ejercicio3.Usuario;
import ppss.ejercicio3.excepciones.JDBCException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Minguez
 */
public class StubReserva extends Reserva {


    private boolean comp;
    
    
    public void setCompruebaPermisos(boolean aux){
        comp = aux;
    }
    
    @Override
    public boolean compruebaPermisos(String login, String password, Usuario tipoUsu){
        return comp;
    }
    
    
}
