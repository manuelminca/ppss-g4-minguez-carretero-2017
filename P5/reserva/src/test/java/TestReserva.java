/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
import ppss.ejercicio3.OperacionFactory;
import ppss.ejercicio3.excepciones.ReservaException;

/**
 *
 * @author Minguez
 */

public class TestReserva {

    public TestReserva() {
    }

    @Test
    public void testRealizaReservaC1() throws Exception {
        try {
            String[] isbns = {"11111"};
            StubReserva res = new StubReserva();
            res.setCompruebaPermisos(false);
            
            dobleOperacion op = new dobleOperacion();
            OperacionFactory.setOperacion(op);

            res.realizaReserva("xxxx", "xxxx", "Luis", isbns);
            fail("debe fallar");
        } catch (ReservaException exc) {
            String expected = "ERROR de permisos; ";
            String actual = exc.getMessage();
            Assert.assertEquals(expected, actual);
        }
    }

    @Test
    public void testRealizaReservaC2() throws Exception {
        String[] isbns = {"11111", "22222"};
        StubReserva res = new StubReserva();
        res.setCompruebaPermisos(true);

        dobleOperacion op = new dobleOperacion();
        OperacionFactory.setOperacion(op);

        res.realizaReserva("ppss", "ppss", "Luis", isbns);

        assertTrue(true);
    }

    @Test
    public void testRealizaReservaC3() throws Exception {
        try {
            String[] isbns = {"33333"};
            StubReserva res = new StubReserva();
            res.setCompruebaPermisos(true);

            dobleOperacion op = new dobleOperacion();
            op.setNum(2);
            OperacionFactory.setOperacion(op);

            res.realizaReserva("ppss", "ppss", "Luis", isbns);

            fail("debe fallar");
        } catch (ReservaException exc) {
            String expected = "ISBN invalido:33333; ";
            String actual = exc.getMessage();
            Assert.assertEquals(expected, actual);
        }

    }

    @Test
    public void testRealizaReservaC4() throws Exception {
        try {
            String[] isbns = {"11111"};
            StubReserva res = new StubReserva();
            res.setCompruebaPermisos(true);

            dobleOperacion op = new dobleOperacion();
            op.setNum(3);
            OperacionFactory.setOperacion(op);

            res.realizaReserva("ppss", "ppss", "Luis", isbns);

            fail("debe fallar");
        } catch (ReservaException exc) {
            String expected = "SOCIO invalido; ";
            String actual = exc.getMessage();
            Assert.assertEquals(expected, actual);
        }

    }
    
        @Test
    public void testRealizaReservaC5() throws Exception {
        try {
            String[] isbns = {"11111"};
            StubReserva res = new StubReserva();
            res.setCompruebaPermisos(true);

            dobleOperacion op = new dobleOperacion();
            op.setNum(4);
            OperacionFactory.setOperacion(op);

            res.realizaReserva("ppss", "ppss", "Luis", isbns);

            fail("debe fallar");
        } catch (ReservaException exc) {
            String expected = "CONEXION invalida; ";
            String actual = exc.getMessage();
            Assert.assertEquals(expected, actual);
        }

    }

}
