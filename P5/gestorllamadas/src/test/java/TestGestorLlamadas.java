/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author ppss
 */
public class TestGestorLlamadas {
    
    public TestGestorLlamadas() {
    }
    
    @Test
    public void testCalculaConsumoC1(){
        double esperado = 208;
        int minutos = 10;
        double result;
        stubgetHoraActual c = new stubgetHoraActual();
        c.setHora(15);
        
        result = c.calculaConsumo(minutos);
        
        Assert.assertEquals(208, result,0.002f);
        
    }
    
        @Test
    public void testCalculaConsumoC2(){
        double esperado = 208;
        int minutos = 10;
        double result;
        stubgetHoraActual c = new stubgetHoraActual();
        c.setHora(22);
        
        result = c.calculaConsumo(minutos);
        
        Assert.assertEquals(105, result,0.002f);
        
    }
    
}
