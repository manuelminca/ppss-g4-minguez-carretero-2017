/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio2;

import ppss.ejercicio2.Calendario;

/**
 *
 * @author Minguez
 */
public class StubCalendario extends Calendario {
    private int hora;

    public void setHora(int nueva) {
        hora = nueva;
    }
 
    @Override
    public int getHoraActual(){
        return hora;
    } 
}
