package Ejercicio2;


import ppss.ejercicio2.Calendario;
import ppss.ejercicio2.GestorLlamadas;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Minguez
 */
public class StubGestorLlamadas extends GestorLlamadas {
    
     private Calendario cal;

    public void setCalendario(Calendario nuevo) {
        cal = nuevo;
    }
 
    @Override
    public Calendario getCalendario(){
        return cal;
    } 
}


