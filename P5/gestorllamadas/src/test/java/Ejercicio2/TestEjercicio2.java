/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio2;

import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
import ppss.ejercicio2.GestorLlamadas;

/**
 *
 * @author Minguez
 */
public class TestEjercicio2 {
    
    public TestEjercicio2() {
    }
    
    @Test
    public void testCalculaConsumoC1(){
        
        StubCalendario cal = new StubCalendario();
        cal.setHora(15);
        StubGestorLlamadas sut = new StubGestorLlamadas();
        sut.setCalendario(cal);
        
        double result;
        int minutos = 10;
        result = sut.calculaConsumo(minutos);
        
        Assert.assertEquals(208, result,0.002f);
        
    }
    
     @Test
    public void testCalculaConsumoC2(){
        
        StubCalendario cal = new StubCalendario();
        cal.setHora(22);
        StubGestorLlamadas sut = new StubGestorLlamadas();
        sut.setCalendario(cal);
        
        double result;
        int minutos = 10;
        result = sut.calculaConsumo(minutos);
        
        Assert.assertEquals(105, result,0.002f);
        
    }
    
    
}
