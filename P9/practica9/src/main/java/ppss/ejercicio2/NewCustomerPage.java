/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 *
 * @author Minguez
 */
public class NewCustomerPage {

    WebDriver driver;
    @FindBy(name = "name")
    WebElement name;
    @FindBy(name = "rad1")
    WebElement gender;
    @FindBy(name = "dob")
    WebElement date;
    @FindBy(name = "addr")
    WebElement address;
    @FindBy(name = "city")
    WebElement city;
    @FindBy(name = "state")
    WebElement state;
    @FindBy(name = "pinno")
    WebElement pin;
    @FindBy(name = "telephoneno")
    WebElement mobileNumber;
    @FindBy(name = "emailid")
    WebElement email;
    @FindBy(name = "password")
    WebElement password;
    @FindBy(name = "sub")
    WebElement submit;
    
    @FindBy(xpath = "//table//p[@class='heading3']")
    WebElement title;
    
    


    public NewCustomerPage(WebDriver driver) {
        this.driver = driver;
    }

    public RegisteredCustomer newCustomer(String name2, String gender2, String date2,
            String address2, String city2,
            String state2, String pin2, String mobileNumber2,
            String email2, String password2) {
        name.sendKeys(name2);
        if (gender2 == "m") {
            driver.findElement(By.cssSelector("input[value='m']")).click();
        } else {
            driver.findElement(By.cssSelector("input[value='f']")).click();
        }

        date.sendKeys(date2);
        address.sendKeys(address2);
        city.sendKeys(city2);
        state.sendKeys(state2);
        pin.sendKeys(pin2);
        mobileNumber.sendKeys(mobileNumber2);
        email.sendKeys(email2);
        password.sendKeys(password2);

        submit.click();
        RegisteredCustomer customer = PageFactory.initElements(driver, RegisteredCustomer.class);
        return customer;

    }
    
    public String getTitle(){
        return title.getText();
    }

}
