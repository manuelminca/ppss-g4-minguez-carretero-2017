/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio2;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ppss.utils.Pausa;

/**
 *
 * @author ppss
 */
public class DeleteCustomerPage {
    WebDriver driver;
    @FindBy(name = "cusid") WebElement customerID;

    @FindBy(xpath = "/html/body/table/tbody/tr/td/table/tbody/tr[1]/td/p") WebElement title;
    @FindBy(name = "AccSubmit") WebElement submit;
    
    public DeleteCustomerPage(WebDriver driver) {
        this.driver = driver;
    }
    
    public String getTitle(){
        return title.getText();
    }
    
    public ManagerPage deleteCustomer(String id){
        customerID.sendKeys(id);
        submit.click();
        Alert alert = driver.switchTo().alert();
        alert.accept();
        Pausa.milisegundos(3000);
        Alert alertDos = driver.switchTo().alert();

        alertDos.accept();
        ManagerPage manager = PageFactory.initElements(driver, ManagerPage.class);
        return manager;
        
      
    }
}
