/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio2;

import ppss.ejercicio1.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 *
 * @author ppss
 */
public class ManagerPage {

    WebDriver driver;
    @FindBy(xpath = "//table//tr[@class='heading3']")
    WebElement homePageUserName;
    @FindBy(linkText = "New Customer")
    WebElement newCustomer;
    @FindBy(linkText = "Log out")
    WebElement logOut;

    public ManagerPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getHomePageDashboardUserName() {
        return homePageUserName.getText();
    }
    
    public NewCustomerPage newCustomer(WebDriver driver){
        newCustomer.click();
        NewCustomerPage customer = PageFactory.initElements(driver, NewCustomerPage.class);
        
        return customer;
        
    }
}

