/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio2;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 *
 * @author ppss
 */
public class RegisteredCustomer {

    WebDriver driver;
    @FindBy(xpath = "//table//p[@class='heading3']")
    WebElement successfully;

    @FindBy(xpath = "//table//tr[4]/td[2]")
    WebElement number;
    @FindBy(linkText = "Delete Customer")
    WebElement delete;
    
        @FindBy(linkText = "Manager")
    WebElement manager;
    
    

    public RegisteredCustomer(WebDriver driver) {
        this.driver = driver;
    }

    public String getSuccessfully() {
        return successfully.getText();
    }

    public String getID() {
        return number.getText();
    }

    public DeleteCustomerPage deleteCustomer() {
        delete.click();
        DeleteCustomerPage delCustomer = PageFactory.initElements(driver, DeleteCustomerPage.class);
        return delCustomer;
    }

    public String getAlert() {
        Alert alert = driver.switchTo().alert();
        String mensaje = alert.getText();
        alert.accept();

        return mensaje;
    }
    
    public ManagerPage goToManager(){
        manager.click();
        ManagerPage mana = PageFactory.initElements(driver, ManagerPage.class);
        return mana;
    }

}
