/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;

import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import ppss.ejercicio1.LoginPage;
import ppss.ejercicio1.ManagerPage;
import ppss.utils.Pausa;

/**
 *
 * @author ppss
 */
public class TestLoginPage {

    WebDriver driver;
    LoginPage poLogin;
    ManagerPage poManagerPage;

    @Before
    public void setup() {
        System.setProperty("webdriver.gecko.driver", "/home/ppss/bin/geckodriver");
        driver = new FirefoxDriver();
        poLogin = PageFactory.initElements(driver, LoginPage.class);
    }

    @Test
    public void test_Login_Correct() {
 
        String loginPageTitle = poLogin.getLoginTitle();

        Assert.assertTrue(loginPageTitle.toLowerCase().contains("guru99 bank"));
        poLogin.login("mngr77105", "zEteqas");
        Pausa.milisegundos(3000);
        poManagerPage = PageFactory.initElements(driver, ManagerPage.class);
        Pausa.milisegundos(3000);
        Assert.assertTrue(poManagerPage.getHomePageDashboardUserName()
                .toLowerCase().contains("manger id : mngr77105"));
        driver.close();
    }

    @Test
    public void test_Login_Incorrect() {
    
        String loginPageTitle = poLogin.getLoginTitle();
        Pausa.milisegundos(5000);
        Assert.assertTrue(loginPageTitle.toLowerCase().contains("guru99 bank"));
        poLogin.login("inco", "inco");
        Pausa.milisegundos(5000);
        String errorMensaje = poLogin.getAlert();
        Assert.assertEquals("User or Password is not valid", errorMensaje);
        driver.close();

    }
    
    

}
