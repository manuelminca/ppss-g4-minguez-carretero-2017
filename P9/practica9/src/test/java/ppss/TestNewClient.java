/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import ppss.ejercicio2.DeleteCustomerPage;
import ppss.ejercicio2.ManagerPage;
import ppss.ejercicio2.LoginPage;
import ppss.ejercicio2.NewCustomerPage;
import ppss.ejercicio2.RegisteredCustomer;
import ppss.utils.Pausa;

/**
 *
 * @author ppss
 */
public class TestNewClient {

    WebDriver driver;
    LoginPage poLogin;
    ManagerPage poManagerPage;
    NewCustomerPage poCustomer;
    RegisteredCustomer poRegistered;
    DeleteCustomerPage poDelete;

    String customerNumber;

    public TestNewClient() {
    }

    @Before
    public void setUp() {
        System.setProperty("webdriver.gecko.driver", "/home/ppss/bin/geckodriver");
        driver = new FirefoxDriver();
        poLogin = PageFactory.initElements(driver, LoginPage.class);
        poLogin.login("mngr77105", "zEteqas");
        Pausa.milisegundos(3000);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void newClientTest() {
        poManagerPage = PageFactory.initElements(driver, ManagerPage.class);

        poCustomer = poManagerPage.newCustomer(driver);
        Pausa.milisegundos(3000);
        assertTrue(poCustomer.getTitle().equals("Add New Customer"));

        poRegistered = poCustomer.newCustomer("mm", "m", "21/06/1996", "Calle x", "Alicante", "Spain", "123456", "999999999", "mo@alu.ua.es", "123456");
        Pausa.milisegundos(3000);
        assertTrue(poRegistered.getSuccessfully().toLowerCase().contains("registered successfully!!!"));
        customerNumber = poRegistered.getID();

        //delete Customer
        
        Pausa.milisegundos(6000);
        poDelete = poRegistered.deleteCustomer();
        Pausa.milisegundos(6000);
        assertTrue(poDelete.getTitle().toLowerCase().contains("delete customer form"));
        poManagerPage = poDelete.deleteCustomer(customerNumber);
        Pausa.milisegundos(6000);
        Assert.assertTrue(poManagerPage.getHomePageDashboardUserName()
                .toLowerCase().contains("manger id : mngr77105"));
        
        Pausa.milisegundos(6000);
        driver.close();
    }
    
    
    @Test
    public void duplicatedClientTest() {
       poManagerPage = PageFactory.initElements(driver, ManagerPage.class);

        poCustomer = poManagerPage.newCustomer(driver);
        Pausa.milisegundos(3000);
        assertTrue(poCustomer.getTitle().equals("Add New Customer"));

        poRegistered = poCustomer.newCustomer("mm", "m", "21/06/1996", "Calle x", "Alicante", "Spain", "123456", "999999999", "mo@alu.ua.es", "123456");
        Pausa.milisegundos(3000);
        assertTrue(poRegistered.getSuccessfully().toLowerCase().contains("registered successfully!!!"));
        customerNumber = poRegistered.getID();
        
        poManagerPage = PageFactory.initElements(driver, ManagerPage.class);

        poCustomer = poManagerPage.newCustomer(driver);
        Pausa.milisegundos(3000);
        assertTrue(poCustomer.getTitle().equals("Add New Customer"));

        poRegistered = poCustomer.newCustomer("mm", "m", "21/06/1996", "Calle x", "Alicante", "Spain", "123456", "999999999", "mo@alu.ua.es", "123456");
        Pausa.milisegundos(3000);


        String mensaje = poRegistered.getAlert();
        
        assertTrue(mensaje.contains("Email Address Already Exist !!"));
        
        
        
        //delete Customer
        
        Pausa.milisegundos(3000);
        poDelete = poRegistered.deleteCustomer();
        Pausa.milisegundos(3000);
        assertTrue(poDelete.getTitle().toLowerCase().contains("delete customer form"));
        poManagerPage = poDelete.deleteCustomer(customerNumber);
        Pausa.milisegundos(3000);
        Assert.assertTrue(poManagerPage.getHomePageDashboardUserName()
                .toLowerCase().contains("manger id : mngr77105"));
        driver.close();
    }


}
